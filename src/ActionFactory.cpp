#include "ActionFactory.h"

BaseActionPtr ActionFactory::createAction( ActionType action_type, Hexagonal::HexGridMapPtr hexgrid_map, Args args ) {
    // 返回的时候没有用引用接收, 导致debug了很长时间
    // Reflector< ActionType, BaseActionFactory > reflector = Reflector< BaseActionFactory >::getReflector();
    Reflector< ActionType, BaseActionFactory >& reflector = Reflector< ActionType, BaseActionFactory >::getReflector();
    BaseActionFactory*                          factory   = reflector.getFactory( action_type );
    if ( factory == nullptr ) {
        return nullptr;
    }
    return BaseActionPtr( factory->create( hexgrid_map, args ) );
}
BaseActionPtr ActionFactory::createAction( int action_type, Hexagonal::HexGridMapPtr hexgrid_map, Args args ) {
    return createAction( static_cast< ActionType >( action_type ), hexgrid_map, args );
}