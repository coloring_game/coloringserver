#include "TurnBasedGame.h"

TurnBasedGame::TurnBasedGame()
    : BaseGame()
    , m_max_round_times( 0 )
    , m_current_round_times( 0 )
    , m_max_turn_times( 0 )
    , m_current_turn_times( 0 )
    , m_max_grid_rate( 0.7 )
    , m_is_single_game( 0 ) {
    ;
}

TurnBasedGame::~TurnBasedGame() {
    ;
}

void TurnBasedGame::paramsInit( Args& args ) {
    BaseGame::paramsInit( args );
    m_max_round_times = args.GetIntArgs( "m_max_round_times" );
    m_max_turn_times  = args.GetIntArgs( "m_max_turn_times" );
    m_max_grid_rate   = args.GetFloatArgs( "m_max_grid_rate" );
    m_is_single_game  = args.GetIntArgs( "m_is_single_game" );
    if ( isSingleGame() ) {
        // 添加单人游戏机器人
        addPlayer( "robot" );
        m_ai = AI::Create( this );
    }
}

StandResponse TurnBasedGame::onAddPlayer( AvatarPtr avatar ) {
    ;
    // 设置玩家Attack为无限次
    avatar->AddAction( ActionType::Attack, -1 );
    // 每次新加玩家都要重设活动队列
    _ResetActiveQueue();
    return StandResponse( true, "" );
}

AvatarPtr TurnBasedGame::nowPlayer() {
    if ( !m_active_queue.empty() ) {
        return m_active_queue.front();
    }
    return nullptr;
}

void TurnBasedGame::nextPlayer() {
    if ( !m_active_queue.empty() ) {
        m_active_queue.pop();
    }
}

bool TurnBasedGame::isNowPlayer( AvatarPtr avatar ) {
    return nowPlayer() == avatar;
}

StandResponse TurnBasedGame::onPlayerAction( AvatarPtr avatar, BaseActionPtr action ) {
    // 给action传入参数
    action->getArgs().SetIntArgs( "check_starthex_turn", 1 );
    action->getArgs().SetIntArgs( "current_turn", nowRound() );
    // 是否轮到改名玩家行动
    if ( !isNowPlayer( avatar ) ) {
        return StandResponse( false, "not " + avatar->Account() + "'s turn" );
    }
    StartTurn();
    StandResponse res = avatar->UseAction( action );
    return res;
}

void clear( std::queue< AvatarPtr >& q ) {
    std::queue< AvatarPtr > empty;
    std::swap( empty, q );
}
void TurnBasedGame::_ResetActiveQueue() {
    clear( m_active_queue );
    while ( !m_active_queue.empty() )
        m_active_queue.pop();
    for ( auto& pair : getPlayerMap() ) {
        m_active_queue.push( pair.second );
    }
}
void TurnBasedGame::onStartGame() {
    // 回合数应该从 1 开始
    m_current_round_times = 1;
    StartRound();
}

void TurnBasedGame::StartRound() {
    onStartRound();
}
void TurnBasedGame::onStartRound() {
    // 当前回合的行动玩家数量应该从 0 开始
    m_current_turn_times = 0;
    _ResetActiveQueue();
    // 在某些回合开始的时候给房间内的玩家添加可用动作
    if ( m_current_round_times == 4 ) {
        for ( auto& pair : getPlayerMap() ) {
            pair.second->AddAction( ActionType::BlockAction, 1 );
        }
    }
    if ( m_current_round_times == 6 ) {
        for ( auto& pair : getPlayerMap() ) {
            pair.second->AddAction( ActionType::ArrowAction, 1 );
        }
    }
    if ( isSingleGame() ) {
        // 机器人有可能在当前回合第一个开始行动
        if ( nowRound() == 1 ) {
            m_ai->firstRoundStrategy();
        }
        else {
            m_ai->otherRoundStrategy();
        }
    }
}

void TurnBasedGame::EndRound() {
    onEndRound();
}

void TurnBasedGame::onEndRound() {
    // 更新行动队列
    _ResetActiveQueue();

    ++m_current_round_times;
    if ( checkGameFinish().m_ok ) {
        EndGame();
    }
    else {
        StartRound();
    }
}

void TurnBasedGame::onStartTurn() {
    ;
}

void TurnBasedGame::onEndTurn() {
    // 当前玩家出队
    nextPlayer();

    ++m_current_turn_times;
    if ( checkRoundFinish().m_ok ) {
        EndRound();
    }
    else {
        StartTurn();
    }
    // 机器人有可能在当前回合玩家行动后开始行动
    if ( isSingleGame() ) {
        if ( nowRound() == 1 ) {
            m_ai->firstRoundStrategy();
        }
        else {
            m_ai->otherRoundStrategy();
        }
    }
}

StandResponse TurnBasedGame::onCheckTurnFinish() {
    return StandResponse( true, "" );
}

StandResponse TurnBasedGame::checkRoundFinish() {
    return onCheckRoundFinish();
}

StandResponse TurnBasedGame::onCheckRoundFinish() {
    // 当前回合所有玩家行动完成即可结束当前回合
    if ( m_current_turn_times == m_max_turn_times ) {
        return StandResponse( true, "check round finish" );
    }
    else {
        return StandResponse( false, "" );
    }
}

StandResponse TurnBasedGame::onCheckGameFinish() {
    Hexagonal::HexGridMapPtr hexgrid_map = getMap();
    // 当前回合为最大回合数+1，即表示最大回合已经结束
    if ( m_current_round_times == m_max_round_times + 1 ) {
        std::string winner = "";

        int max_score = 0;

        for ( auto& player : getPlayerMap() ) {
            int score = getMap()->GridColorNumber( player.second->Color() );
            if ( score > max_score ) {
                max_score = score;
                winner    = player.second->Account();
            }
        }
        return StandResponse( true, winner );
    }
    for ( auto& player : getPlayerMap() ) {
        int color_count = hexgrid_map->GridColorNumber( player.second->Color() );
        // 有玩家已染色格子数量大于阈值检查
        if ( color_count >= static_cast< int >( hexgrid_map->GridNumber() * m_max_grid_rate ) ) {
            return StandResponse( true, "winner is " + player.second->Account() );
        }
        // 玩家染色格数为0检查, 为0即结束游戏 (3人及以上人数游玩时有bug)
        if ( color_count == 0 && m_current_round_times > 1 ) {
            return StandResponse( true, "loser is " + player.second->Account() );
        }
    }
    return StandResponse( false, "game has not finished" );
}