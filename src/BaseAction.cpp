#include "BaseAction.h"

BaseAction::BaseAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args )
    : m_hexgrid_map( hexgrid_map )
    , m_args( args )
    , m_color( args.GetStringArgs( "color" ) ) {
    ;
};
BaseAction::~BaseAction() {
    ;
};

StandResponse BaseAction::doAction() {
    return onDoAction();
};

StandResponse BaseAction::onDoAction() {
    return StandResponse( true, "ok" );
};

Args& BaseAction::getArgs() {
    return m_args;
}

Hexagonal::HexGridMapPtr BaseAction::getMap() {
    return m_hexgrid_map;
}

StandResponse CheckStartHexInterface::checkStartHex( Hexagonal::Hex start_hex, Hexagonal::HexGridMapPtr hexgrid_map, std::string color ) {
    if ( hexgrid_map == nullptr ) {
        return StandResponse( false, "hexgrid_map is nullptr" );
    }
    Hexagonal::MyGrid* grid = hexgrid_map->GetGrid( start_hex );
    if ( grid == nullptr ) {
        return StandResponse( false, "error startHex" );
    }
    if ( grid->m_color == color ) {
        return StandResponse( true, "checkStartHex ok" );
    }
    // 六个方向
    for ( int i = 0; i < 6; ++i ) {
        Hexagonal::MyGrid* hex_grid = hexgrid_map->GetGrid( Hexagonal::HexFunction::hex_neighbor( start_hex, i ) );
        if ( hex_grid == nullptr ) {
            continue;
        }
        if ( hex_grid->m_color == color ) {
            return StandResponse( true, "checkStartHex ok" );
        }
    }
    return StandResponse( false, "checkStartHex failed" );
}