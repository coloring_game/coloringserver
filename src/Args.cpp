#include "Args.h"
void Args::SetStringArgs( std::string key, std::string value ) {
    string_args_map[ key ] = value;
}
std::string Args::GetStringArgs( std::string key ) {
    return string_args_map.at( key );
}

void Args::SetIntArgs( std::string key, int value ) {
    int_args_map[ key ] = value;
}
int Args::GetIntArgs( std::string key ) {
    return int_args_map.at( key );
}

void Args::SetFloatArgs( std::string key, float value ) {
    float_args_map[ key ] = value;
}
float Args::GetFloatArgs( std::string key ) {
    return float_args_map.at( key );
}
