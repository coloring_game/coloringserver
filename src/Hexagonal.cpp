#include "Hexagonal.h"

using namespace Hexagonal;

Hex::Hex( int q_, int r_ )
    : q( q_ )
    , r( r_ )
    , s( -q_ - r_ ) {
    ;
}
Hex::Hex( int q_, int r_, int s_ )
    : q( q_ )
    , r( r_ )
    , s( s_ ) {
    if ( q + r + s != 0 )
        throw "q + r + s must equal 0";
}
Hex::Hex( double q_, double r_ ) {
    int qi = int( round( q_ ) );
    int ri = int( round( r_ ) );
    int si = -qi - ri;
    q      = qi;
    r      = ri;
    s      = si;
}
Hex::Hex( double q_, double r_, double s_ ) {
    int    qi     = int( round( q_ ) );
    int    ri     = int( round( r_ ) );
    int    si     = int( round( s_ ) );
    double q_diff = abs( qi - q_ );
    double r_diff = abs( ri - r_ );
    double s_diff = abs( si - s_ );
    if ( q_diff > r_diff && q_diff > s_diff ) {
        qi = -ri - si;
    }
    else {
        if ( r_diff > s_diff ) {
            ri = -qi - si;
        }
        else {
            si = -qi - ri;
        }
    }
    q = qi;
    r = ri;
    s = si;
    if ( q + r + s != 0 )
        throw "q + r + s must equal 0";
}
std::ostream& operator<<( std::ostream& out, const Hex& hex ) {
    out << "(" << hex.q << "," << hex.r << "," << hex.s << ")";
    return out;
}

static const std::vector< Hex > hex_directions = { Hex( 1, 0, -1 ), Hex( 1, -1, 0 ), Hex( 0, -1, 1 ), Hex( -1, 0, 1 ), Hex( -1, 1, 0 ), Hex( 0, 1, -1 ) };
static const std::vector< Hex > hex_diagonals  = { Hex( 2, -1, -1 ), Hex( 1, -2, 1 ), Hex( -1, -1, 2 ), Hex( -2, 1, 1 ), Hex( -1, 2, -1 ), Hex( 1, 1, -2 ) };

Hex HexFunction::hex_add( const Hex& a, const Hex& b ) {
    return Hex( a.q + b.q, a.r + b.r, a.s + b.s );
}

Hex HexFunction::hex_subtract( const Hex& a, const Hex& b ) {
    return Hex( a.q - b.q, a.r - b.r, a.s - b.s );
}

Hex HexFunction::hex_scale( const Hex& a, int k ) {
    return Hex( a.q * k, a.r * k, a.s * k );
}

Hex HexFunction::hex_rotate_left( const Hex& a ) {
    return Hex( -a.s, -a.q, -a.r );
}

Hex HexFunction::hex_rotate_right( const Hex& a ) {
    return Hex( -a.r, -a.s, -a.q );
}

Hex HexFunction::hex_neighbor( const Hex& hex, int direction ) {
    return HexFunction::hex_add( hex, hex_directions[ direction ] );
}

Hex HexFunction::hex_diagonal_neighbor( const Hex& hex, int direction ) {
    return HexFunction::hex_add( hex, hex_diagonals[ direction ] );
}

int HexFunction::hex_length( const Hex& hex ) {
    return int( ( abs( hex.q ) + abs( hex.r ) + abs( hex.s ) ) / 2 );
}

int HexFunction::hex_distance( const Hex& a, const Hex& b ) {
    return HexFunction::hex_length( hex_subtract( a, b ) );
}

Hex HexFunction::hex_lerp( const Hex& a, const Hex& b, double t ) {
    return Hex( a.q * ( 1.0 - t ) + b.q * t, a.r * ( 1.0 - t ) + b.r * t, a.s * ( 1.0 - t ) + b.s * t );
}

std::vector< Hex > hex_linedraw( const Hex& a, const Hex& b ) {
    int N = HexFunction::hex_distance( a, b );

    Hex a_nudge = Hex( a.q + 1e-06, a.r + 1e-06, a.s - 2e-06 );
    Hex b_nudge = Hex( b.q + 1e-06, b.r + 1e-06, b.s - 2e-06 );

    std::vector< Hex > results = {};
    double             step    = 1.0 / std::max( N, 1 );
    for ( int i = 0; i <= N; i++ ) {
        results.push_back( HexFunction::hex_lerp( a_nudge, b_nudge, step * i ) );
    }
    return results;
}

MyGrid::MyGrid( int q, int r )
    : m_pos( q, r )
    , m_avoid_times( AVOID_TIMES )
    , m_color( DEFAULT_COLOR )
    , m_visited_times( 0 )
    , m_is_need_border( false ) {
    // ParamsInit();
}

MyGrid::MyGrid( const Hex& hex )
    : m_pos( hex )
    , m_avoid_times( AVOID_TIMES )
    , m_color( DEFAULT_COLOR )
    , m_visited_times( 0 )
    , m_is_need_border( false ) {
    // ParamsInit();
}

void MyGrid::ParamsInit() {
    m_avoid_times    = AVOID_TIMES;
    m_color          = DEFAULT_COLOR;
    m_visited_times  = 0;
    m_is_need_border = false;
}

void MyGrid::Active( std::string color, bool is_border ) {
    // 颜色激活时要重设时间
    m_avoid_times    = AVOID_TIMES;
    m_color          = color;
    m_is_need_border = is_border;
}

void MyGrid::FinishOneRound() {
    --m_avoid_times;
}

bool MyGrid::IsNeedBorder() {
    return m_is_need_border;
}

void MyGrid::SetBorder() {
    m_is_need_border = true;
}

void MyGrid::ClearBorder() {
    m_is_need_border = false;
}

std::string MyGrid::getStrHex() {
    return std::to_string( m_pos.q ) + "," + std::to_string( m_pos.r );
}

HexGridMap::HexGridMap() {
    m_hexgrid_map.resize( 2 * m_radius + 1 );
    for ( int q = -m_radius; q <= m_radius; ++q ) {
        for ( int r = -m_radius; r <= m_radius; ++r ) {
            if ( q + r <= m_radius && q + r >= -m_radius ) {
                MyGrid* grid = new MyGrid( q, r );
                m_hexgrid_map[ q + m_radius ].push_back( grid );
            }
            else {
                m_hexgrid_map[ q + m_radius ].push_back( nullptr );
            }
        }
    }
}

HexGridMap::~HexGridMap() {
    LOG( INFO ) << "~HexGridMap()";
}

std::vector< std::vector< MyGrid* > > HexGridMap::GetMap() {
    return m_hexgrid_map;
}

MyGrid* HexGridMap::GetGrid( int q, int r ) {
    if ( q > m_radius || q < -m_radius || r > m_radius || r < -m_radius ) {
        return nullptr;
    }
    // TODO 这里需要改为at,并进行测试
    return m_hexgrid_map[ q + m_radius ][ r + m_radius ];
}
MyGrid* HexGridMap::GetGrid( const Hex& hex ) {
    return GetGrid( hex.q, hex.r );
}

//清除某个颜色的行动标记
void HexGridMap::ClearMapBorder( std::string color ) {
    auto func = std::bind( &HexGridMap::_ClearIsNeedBorder, this, std::placeholders::_1, std::placeholders::_2, color );
    _IteratorAllGrid( func );
}

void HexGridMap::SetMapColor( std::vector< MyGrid* > grid_vector, std::string color, bool check_avoid_times ) {
    for ( auto grid : grid_vector ) {
        if ( check_avoid_times ) {
            if ( grid->m_avoid_times <= 0 ) {
                grid->Active( color, true );
            }
        }
        else {
            grid->Active( color, true );
        }
    }
    unsigned long long t1 = get_now_millisecond();
    CheckSurround( color );
    unsigned long long t2 = get_now_millisecond();
    LOG( INFO ) << "CheckSurround used " << t2 - t1 << " ms";
}

unsigned long long HexGridMap::get_now_millisecond() {
    struct timespec ts;
    clock_gettime( CLOCK_MONOTONIC_RAW, &ts );
    return ts.tv_sec * 1000 + ts.tv_nsec / ( 1000 * 1000 );
}

unsigned char HexGridMap::dfs( int q, int r, std::string color, int flood_times ) {
    if ( q < -m_radius )
        return 1 << 0;
    if ( q > m_radius )
        return 1 << 1;
    if ( r < -m_radius )
        return 1 << 2;
    if ( r > m_radius )
        return 1 << 3;
    int s = -q - r;
    if ( s < -m_radius )
        return 1 << 4;
    if ( s > m_radius )
        return 1 << 5;

    MyGrid* my_grid = GetGrid( q, r );
    if ( my_grid->m_visited_times == flood_times || my_grid->m_color == color )
        return 0;

    my_grid->m_visited_times = flood_times;
    unsigned char res1       = dfs( q + 1, r, color, flood_times );
    unsigned char res2       = dfs( q + 1, r - 1, color, flood_times );
    unsigned char res3       = dfs( q, r - 1, color, flood_times );
    unsigned char res4       = dfs( q - 1, r, color, flood_times );
    unsigned char res5       = dfs( q - 1, r + 1, color, flood_times );
    unsigned char res6       = dfs( q, r + 1, color, flood_times );
    ;
    unsigned char res = res1 | res2 | res3 | res4 | res5 | res6;
    return res;
}
// 当设置某个颜色时, 判断是否形成包围
// 如果从一个空白点出发, 只碰到了2条边界或2天以下边界, 就意味着被包围了
bool HexGridMap::CheckSurround( std::string color ) {
    std::string tmp_color = "xxx";
    // 遍历所有点
    int flood_times = 1;
    for ( int q = -m_radius; q <= m_radius; ++q ) {
        for ( int r = -m_radius; r <= m_radius; ++r ) {
            if ( q + r <= m_radius && q + r >= -m_radius ) {
                // 判断颜色不一样的grid是否被颜色为color的grid包围
                MyGrid* my_grid = GetGrid( q, r );
                if ( my_grid != nullptr && my_grid->m_color != color && my_grid->m_visited_times < flood_times ) {
                    // 找到了flood起始点
                    unsigned char res = dfs( q, r, color, flood_times );
                    if ( CountTable[ res ] <= 2 ) {
                        // 以flood为起始点的区域可以被包围,
                        // 因此把m_visited_times==flood_times的所有区域染色
                        Args args;
                        args.SetStringArgs( "color", color );
                        args.SetIntArgs( "flood_times", flood_times );
                        auto func = std::bind( &HexGridMap::_SetColorByFloodTimes, this, std::placeholders::_1, std::placeholders::_2, args );
                        _IteratorAllGrid( func );
                    }
                    ++flood_times;
                }
            }
        }
    }
    auto func = std::bind( &HexGridMap::_ResetVisitedTimes, this, std::placeholders::_1, std::placeholders::_2 );
    _IteratorAllGrid( func );
    return true;
}

// 内部函数, 用于遍历所有Grid
void HexGridMap::_IteratorAllGrid( std::function< void( int, int ) > func ) {
    for ( int q = -m_radius; q <= m_radius; ++q ) {
        for ( int r = -m_radius; r <= m_radius; ++r ) {
            if ( q + r <= m_radius && q + r >= -m_radius ) {
                func( q, r );
            }
        }
    }
}

void HexGridMap::_ResetVisitedTimes( int q, int r ) {
    MyGrid* my_grid = GetGrid( q, r );
    if ( my_grid != nullptr ) {
        my_grid->m_visited_times = 0;
    }
}
//清除某个颜色的行动标记
void HexGridMap::_ClearIsNeedBorder( int q, int r, std::string color ) {
    MyGrid* my_grid = GetGrid( q, r );
    if ( my_grid != nullptr && my_grid->m_color == color ) {
        my_grid->ClearBorder();
    }
}
void HexGridMap::_SetColorByFloodTimes( int q, int r, Args args ) {
    std::string color       = args.GetStringArgs( "color" );
    int         flood_times = args.GetIntArgs( "flood_times" );

    MyGrid* my_grid = GetGrid( q, r );
    if ( my_grid != nullptr && my_grid->m_visited_times == flood_times ) {
        my_grid->m_color = color;
    }
}

void HexGridMap::_SubAvoidTimes( int q, int r ) {
    MyGrid* my_grid = GetGrid( q, r );
    if ( my_grid != nullptr ) {
        --my_grid->m_avoid_times;
    }
}

void HexGridMap::SubAvoidTimes() {
    auto func = std::bind( &HexGridMap::_SubAvoidTimes, this, std::placeholders::_1, std::placeholders::_2 );
    _IteratorAllGrid( func );
}

bool HexGridMap::CheckStartHex( const Hex& start_hex ) {
    return true;
}

// 各种道具
std::vector< MyGrid* > HexGridMap::Attack( const Hex& start_hex, int direction ) {
    std::vector< MyGrid* > res;
    _res_push_back( res, start_hex );
    _res_push_back( res, HexFunction::hex_neighbor( start_hex, direction ) );
    return res;
}
std::vector< MyGrid* > HexGridMap::Arrow( const Hex& start_hex, int direction, int length ) {
    std::vector< MyGrid* > res;
    _res_push_back( res, start_hex );
    Hex tmp = HexFunction::hex_neighbor( start_hex, direction );
    for ( int i = 0; i < length; ++i ) {
        _res_push_back( res, tmp );
        tmp = HexFunction::hex_neighbor( tmp, direction );
    }
    return res;
}
std::vector< MyGrid* > HexGridMap::HexBlock( const Hex& start_hex, int length ) {
    std::vector< MyGrid* > res;
    _res_push_back( res, start_hex );
    for ( int q = -length; q <= length; ++q ) {
        for ( int r = std::max( -length, -q - length ); r <= std::min( length, -q + length ); ++r ) {
            _res_push_back( res, HexFunction::hex_add( start_hex, Hex( q, r ) ) );
        }
    }
    return res;
}

std::vector< MyGrid* > HexGridMap::Triangle( const Hex& start_hex, int direction, int length ) {
    std::vector< MyGrid* > res;
    _res_push_back( res, start_hex );
    for ( int q = 0; q <= length; ++q ) {
        for ( int r = 0; r <= length - q; ++r ) {
            // 排除起始点
            if ( q == 0 && r == 0 ) {
                continue;
            }
            // 顺时针旋转n次
            Hex tmp_hex = Hex( q, r );
            for ( int i = 0; i < direction; ++i ) {
                tmp_hex = HexFunction::hex_rotate_left( tmp_hex );
            }
            _res_push_back( res, HexFunction::hex_add( start_hex, tmp_hex ) );
        }
    }
    return res;
}

int HexGridMap::GridColorNumber( std::string color ) {
    int color_number = 0;
    for ( int q = -m_radius; q <= m_radius; ++q ) {
        for ( int r = -m_radius; r <= m_radius; ++r ) {
            if ( q + r <= m_radius && q + r >= -m_radius ) {
                MyGrid* my_grid = GetGrid( q, r );
                if ( my_grid != nullptr && my_grid->m_color == color ) {
                    ++color_number;
                }
            }
        }
    }
    return color_number;
}

std::vector< Hex > HexGridMap::GridColorVector( std::string color ) {
    std::vector< Hex > res;
    for ( int q = -m_radius; q <= m_radius; ++q ) {
        for ( int r = -m_radius; r <= m_radius; ++r ) {
            if ( q + r <= m_radius && q + r >= -m_radius ) {
                MyGrid* my_grid = GetGrid( q, r );
                if ( my_grid != nullptr && my_grid->m_color == color ) {
                    res.push_back( Hex( q, r ) );
                }
            }
        }
    }
    return res;
}

int HexGridMap::GridNumber() {
    return m_grid_number;
}

void HexGridMap::_res_push_back( std::vector< MyGrid* >& grid_vector, const Hex& hex ) {
    MyGrid* tmp = GetGrid( hex );
    if ( tmp != nullptr ) {
        grid_vector.push_back( tmp );
    }
}