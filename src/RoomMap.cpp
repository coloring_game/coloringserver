#include "RoomMap.h"
#include "Timer.h"
extern TimerManager timer_manager;
GameManagerMap::GameManagerMap()
    : next_room_id( 1 ) {
    ;
}

int GameManagerMap::CreateRoom( int type, int player_num ) {
    std::lock_guard< std::mutex > lock( m_mutex );
    // m_map为空则直接插入, 不为空则检查room_id是否重复
    if ( !m_map.empty() ) {
        for ( auto it = m_map.find( next_room_id ); it != m_map.end(); ++next_room_id, ++it ) {
            ;
        }
    }

    TurnBasedGamePtr gamePtr = TurnBasedGame::Create();
    Args             args;
    args.SetIntArgs( "m_max_player_num", player_num );
    args.SetIntArgs( "m_max_round_times", 10 );
    args.SetIntArgs( "m_max_turn_times", player_num );
    args.SetFloatArgs( "m_max_grid_rate", 0.65 );
    args.SetIntArgs( "m_is_single_game", type );
    gamePtr->paramsInit( args );
    m_map[ next_room_id ] = gamePtr;

    return next_room_id++;
}

bool GameManagerMap::EnterRoom( int room_id, const ClientObject& client ) {
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( room_id );
    if ( it != m_map.end() ) {
        TurnBasedGamePtr gm = it->second;
        return gm->addPlayer( client.Account() ).m_ok;
    }
    else {
        return true;
    }
}
bool GameManagerMap::CloseRoom( int room_id ) {
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( room_id );
    if ( it != m_map.end() ) {
        m_map.erase( it );
        return true;
    }
    else {
        return false;
    }
}
bool GameManagerMap::ClearEndRoom() {
    std::lock_guard< std::mutex > lock( m_mutex );
    // std::cout << "GameManagerMap::ClearEndRoom()" << std::endl;
    for ( auto it = m_map.begin(); it != m_map.end(); ) {
        if ( it->second->isEnd() ) {
            m_map.erase( it++ );
        }
        else {
            ++it;
        }
    }
    auto func = std::bind( &GameManagerMap::ClearEndRoom, this );
    timer_manager.add_timer( 10 * 1000, func );
    return true;
}
bool GameManagerMap::QuitRoom( int room_id, const ClientObject& client ) {
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( room_id );
    if ( it != m_map.end() ) {
        m_map.erase( it );
        return true;
    }
    else {
        return false;
    }
}
TurnBasedGamePtr GameManagerMap::GetRoomPtr( int room_id ) {
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( room_id );
    if ( it != m_map.end() ) {
        return it->second;
    }
    else {
        return nullptr;
    }
}
