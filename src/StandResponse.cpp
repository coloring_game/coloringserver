#include "StandResponse.h"

StandResponse::StandResponse( bool ok, std::string msg )
    : m_ok( ok )
    , m_msg( msg ) {
    std::cout << "ok:" << m_ok << " msg:" << m_msg << std::endl;
    // LOG( INFO ) << "ok:" << m_ok << " msg:" << m_msg;
}
