#include "BaseGame.h"

BaseGame::BaseGame()
    : m_max_player_num( 0 )
    , m_current_player_number( 0 )
    , m_status( GameStatus::INIT )
    , m_color_list( { "red", "bule", "yellow", "green" } )
    , m_hexgrid_map( Hexagonal::HexGridMap::Create() ) {
    LOG( INFO ) << "BaseGame::BaseGame()";
}

BaseGame::~BaseGame() {
    LOG( INFO ) << "BaseGame::~BaseGame()";
}

void BaseGame::paramsInit( Args& args ) {
    m_max_player_num = args.GetIntArgs( "m_max_player_num" );
    m_status         = GameStatus::PREPARE;
}

StandResponse BaseGame::onAddPlayer( AvatarPtr avatar ) {
    return StandResponse( true, "onAddPlayer" );
}
StandResponse BaseGame::addPlayer( const std::string& account ) {
    // 重连处理
    auto it = m_player_map.find( account );
    if ( it != m_player_map.end() ) {
        return StandResponse( true, "player already exist, reconnecting succeed" );
    }

    // 准备阶段才能加入玩家
    if ( m_status != GameStatus::PREPARE ) {
        return StandResponse( false, "player can only enter room while room's status equals PREPARE" );
    }

    AvatarPtr player = Avatar::Create( account );
    player->SetColor( m_color_list.back() );
    m_color_list.pop_back();
    m_player_map[ account ] = player;
    ++m_current_player_number;

    onAddPlayer( player );

    if ( m_current_player_number == m_max_player_num ) {
        StartGame();
    }

    StandResponse res = StandResponse( true, "add player succeed : " + account );
    return res;
}
StandResponse BaseGame::onDeletePlayer( const std::string& account ) {
    return StandResponse( true, "onDeletePlayer" );
}
StandResponse BaseGame::deletePlayer( const std::string& account ) {
    auto it = m_player_map.find( account );
    if ( it == m_player_map.end() ) {
        return StandResponse( true, "player not exist" );
    }
    m_player_map.erase( it );
    onDeletePlayer( account );
    return StandResponse( true, "add player succeed : " + account );
}
StandResponse BaseGame::onPlayerAction( AvatarPtr avatar, BaseActionPtr action ) {

    StandResponse res = avatar->UseAction( action );

    return res;
}

StandResponse BaseGame::playerAction( const std::string& account, int action_type, Args args ) {
    // 游戏是否处于运行状态
    if ( getStatus() != GameStatus::RUNNING ) {
        return StandResponse( false, "game not in running status" );
    }

    auto it = m_player_map.find( account );
    if ( it == m_player_map.end() ) {
        return StandResponse( false, "player not exist" );
    }
    AvatarPtr player = m_player_map[ account ];
    args.SetStringArgs( "color", player->Color() );
    BaseActionPtr action = ActionFactory::createAction( static_cast< ActionType >( action_type ), getMap(), args );
    if ( action == nullptr ) {
        return StandResponse( false, "invalid action type:" + std::to_string( action_type ) );
    }
    StandResponse res = onPlayerAction( player, action );
    if ( !res.m_ok ) {
        return res;
    }

    if ( checkTurnFinish().m_ok ) {
        EndTurn();
    }

    if ( checkGameFinish().m_ok ) {
        EndGame();
    }
    return res;
}
void BaseGame::onStartGame() {
    ;
}
void BaseGame::StartGame() {
    m_status = GameStatus::RUNNING;

    onStartGame();
}
void BaseGame::onEndGame() {
    ;
}
void BaseGame::EndGame() {
    m_status = GameStatus::END;
    onEndGame();
}
void BaseGame::onStartTurn() {
    ;
}
void BaseGame::StartTurn() {
    onStartTurn();
}
void BaseGame::onEndTurn() {
    ;
}
void BaseGame::EndTurn() {
    onEndTurn();
}
StandResponse BaseGame::onCheckGameFinish() {
    return StandResponse( false, "not implemented" );
}
StandResponse BaseGame::checkGameFinish() {
    return onCheckGameFinish();
}
StandResponse BaseGame::onCheckTurnFinish() {
    return StandResponse( false, "not implemented" );
}
StandResponse BaseGame::checkTurnFinish() {
    return onCheckTurnFinish();
}

GameStatus& BaseGame::getStatus() {
    return m_status;
}
Hexagonal::HexGridMapPtr BaseGame::getMap() {
    return m_hexgrid_map;
}
AvatarPtr BaseGame::getPlayer( const std::string& account ) {
    auto it = m_player_map.find( account );
    if ( it != m_player_map.end() ) {
        return it->second;
    }
    return nullptr;
}