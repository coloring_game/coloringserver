#include "Timer.h"

#include "BaseGame.h"
#include "StandResponse.h"
#include "main.h"
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include "Avatar.h"
#include "BattleService.h"
#include "ClientObject.h"
#include "RoleService.h"
#include "RoomService.h"
#include "Timer.h"
#include "WebSocketService.h"
#include <stdlib.h>  //sleep
#include <unistd.h>  //usleep
// Logic and data behind the server's behavior.
ClientMap      client_map;
TimerManager   timer_manager;
GameManagerMap room_map;
// HexGridStorage hex_grid_storage(10);
using namespace cinatra;
void RunServer() {
    http_server server( std::thread::hardware_concurrency() );
    server.listen( "0.0.0.0", "44584" );

    RoleService      role_service;
    RoomService      room_service;
    BattleService    battle_service;
    WebSocketService web_socket_service;

    std::function< void( request&, response& ) > func;

    func = std::bind( &RoleService::REST, &role_service, std::placeholders::_1, std::placeholders::_2 );
    server.set_http_handler< POST, GET, OPTIONS >( "/role", func );

    func = std::bind( &RoomService::REST, &room_service, std::placeholders::_1, std::placeholders::_2 );
    server.set_http_handler< POST, DEL, GET, OPTIONS >( "/room", func );

    func = std::bind( &BattleService::REST, &battle_service, std::placeholders::_1, std::placeholders::_2 );
    server.set_http_handler< POST, OPTIONS >( "/battle", func );

    func = std::bind( &BattleService::REST, &web_socket_service, std::placeholders::_1, std::placeholders::_2 );
    server.set_http_handler< GET >( "/ws", func );

    server.run();
}

void TimerManagerLoop() {

    while ( true ) {
        // std::cout << "TimerManagerLoop" << std::endl;
        usleep( 1000000 );
        timer_manager.tick();
    }
}

int main( int argc, char** argv ) {
    // BaseGame* game = new BaseGame();
    google::InitGoogleLogging( argv[ 0 ] );
    FLAGS_log_dir = "../log";
    // 设置日志缓冲区级别, 让info类型的日子也可以直接输出
    FLAGS_logbuflevel = -1;
    LOG( INFO ) << "InitGoogleLogging ";
    std::thread th1( RunServer );
    std::thread th3( TimerManagerLoop );
    auto        func = std::bind( &GameManagerMap::ClearEndRoom, &room_map );
    timer_manager.add_timer( 10 * 1000, func );
    th1.join();
    // th2.join();
    th3.join();

    google::ShutdownGoogleLogging();

    return 0;
}