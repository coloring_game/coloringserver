#include "StartHexAction.h"

StartHexAction::StartHexAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args )
    : BaseAction( hexgrid_map, args )
    , m_start_hex( args.GetIntArgs( "q" ), args.GetIntArgs( "r" ) )
    , m_direction( args.GetIntArgs( "direction" ) ) {
    ;
}

StartHexAction::~StartHexAction() {
    ;
}

StandResponse StartHexAction::doAction() {
    if ( getDirection() < 0 || getDirection() > 5 ) {
        return StandResponse( false, "direction error" );
    }
    if ( getMap()->GetGrid( getStartHex() ) == nullptr ) {
        return StandResponse( false, "error start hex" );
    }

    // 检查起点连地规则需要排除第一回合
    if ( isNeedCheckStartHex() ) {
        StandResponse response = checkStartHex( getStartHex(), getMap(), getColor() );
        if ( response.m_ok == false ) {
            return response;
        }
    }
    // 第一回合不能占领敌方地盘
    if ( getArgs().GetIntArgs( "current_turn" ) == 1 ) {
        std::string grid_color = getMap()->GetGrid( getStartHex() )->m_color;
        if ( grid_color != "white" && grid_color != getColor() ) {
            return StandResponse( false, "first turn can't capture enemy grid" );
        }
    }
    return onDoAction();
}
