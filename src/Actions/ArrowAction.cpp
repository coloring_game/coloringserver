#include "Actions/ArrowAction.h"
REGISTER_BASEACTION( ArrowAction );
ArrowAction::ArrowAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args )
    : StartHexAction( hexgrid_map, args ) {
    ;
}

ArrowAction::~ArrowAction() {
    ;
}

StandResponse ArrowAction::onDoAction() {

    Hexagonal::HexGridMapPtr hexgrid_map = getMap();

    std::vector< Hexagonal::MyGrid* > res;
    hexgrid_map->_res_push_back( res, getStartHex() );
    Hexagonal::Hex tmp = Hexagonal::HexFunction::hex_neighbor( getStartHex(), getDirection() );
    for ( int i = 0; i < 3; ++i ) {
        hexgrid_map->_res_push_back( res, tmp );
        tmp = Hexagonal::HexFunction::hex_neighbor( tmp, getDirection() );
    }
    hexgrid_map->SetMapColor( res, getColor(), false );

    return StandResponse( true, "ok" );
}
