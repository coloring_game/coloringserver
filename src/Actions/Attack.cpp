#include "Actions/Attack.h"
REGISTER_BASEACTION( Attack );
Attack::Attack( Hexagonal::HexGridMapPtr hexgrid_map, Args args )
    : StartHexAction( hexgrid_map, args ) {
    ;
}

Attack::~Attack() {
    ;
}

StandResponse Attack::onDoAction() {
    Hexagonal::HexGridMapPtr hexgrid_map = getMap();

    std::vector< Hexagonal::MyGrid* > res;
    hexgrid_map->_res_push_back( res, getStartHex() );
    hexgrid_map->_res_push_back( res, Hexagonal::HexFunction::hex_neighbor( getStartHex(), getDirection() ) );

    hexgrid_map->SetMapColor( res, getColor(), false );
    return StandResponse( true, "ok" );
}
