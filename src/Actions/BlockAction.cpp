#include "Actions/BlockAction.h"
REGISTER_BASEACTION( BlockAction );
BlockAction::BlockAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args )
    : StartHexAction( hexgrid_map, args ) {
    ;
}

BlockAction::~BlockAction() {
    ;
}

StandResponse BlockAction::onDoAction() {

    Hexagonal::HexGridMapPtr hexgrid_map = getMap();

    std::vector< Hexagonal::MyGrid* > res;
    hexgrid_map->_res_push_back( res, getStartHex() );
    Hexagonal::Hex tmp    = Hexagonal::HexFunction::hex_neighbor( getStartHex(), getDirection() );
    int            length = 1;
    for ( int q = -length; q <= length; ++q ) {
        for ( int r = std::max( -length, -q - length ); r <= std::min( length, -q + length ); ++r ) {
            hexgrid_map->_res_push_back( res, Hexagonal::HexFunction::hex_add( getStartHex(), Hexagonal::Hex( q, r ) ) );
        }
    }
    hexgrid_map->SetMapColor( res, getColor(), false );

    return StandResponse( true, "ok" );
}
