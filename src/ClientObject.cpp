#include "ClientObject.h"

ClientObject::ClientObject( std::string account ) {
    m_account    = account;
    m_color      = "bule";
    m_timer      = nullptr;
    is_can_do    = false;
    m_connection = nullptr;
}
bool ClientObject::operator==( ClientObject client ) {
    return ( client.m_account == m_account );
}
void ClientObject::SetTimer( Timer* timer ) {
    m_timer = timer;
}
Timer*& ClientObject::GetTimer() {
    return m_timer;
}
void ClientObject::SetColor( std::string color ) {
    m_color = color;
}
const std::string& ClientObject::Color() const {
    return m_color;
}
const std::string& ClientObject::Account() const {
    return m_account;
}
void ClientObject::SetConnection( request& req ) {
    auto conn    = req.get_conn< cinatra::NonSSL >();
    m_connection = conn;
}
bool ClientObject::HasConnection() {
    return ( m_connection != nullptr );
}
void ClientObject::ClearConnection() {
    m_connection = nullptr;
}
void ClientObject::SendMessage( std::string msg ) {
    if ( HasConnection() ) {
        try {
            std::cout << m_account << " " << msg << std::endl;
            m_connection->send_ws_string( std::move( msg ) );
        }
        catch ( ... ) {
            // 发送消息给客户端失败, 说明连接关闭了
            ClearConnection();
        }
    }
}
ClientMap::ClientMap() {
    ;
}

ClientObject* ClientMap::get( std::string account, bool is_need_check_connection ) {
    LOG( INFO ) << "ClientMap::get " + account;
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( account );
    if ( it != m_map.end() ) {
        if ( is_need_check_connection && it->second->HasConnection() ) {
            return it->second;
        }
        else {
            return it->second;
        }
    }
    return nullptr;
}

bool ClientMap::find( std::string account ) {
    // LOG( INFO ) << m_map.size() ;
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( account );
    if ( it != m_map.end() ) {
        if ( it->second->HasConnection() ) {
            return true;
        }
    }
    return false;
}

bool ClientMap::set( std::string account, ClientObject* client ) {
    // LOG( INFO ) << m_map.size() ;
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( account );
    if ( it == m_map.end() ) {
        // 设置每个 client 的颜色
        m_map[ account ] = client;
        return true;
    }
    return false;
}
bool ClientMap::remove( std::string account ) {
    LOG( INFO ) << "ClientMap::remove";
    std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_map.find( account );
    if ( it != m_map.end() ) {
        m_map.erase( it );
        return true;
    }
    return false;
}
void ClientMap::sendMsg( std::string msg ) {
    std::cout << "ClientMap::sendMsg " << msg << std::endl;
    std::lock_guard< std::mutex > lock( m_mutex );
    for ( auto& it : m_map ) {
        it.second->SendMessage( msg );
    }
}