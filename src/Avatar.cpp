#include "Avatar.h"

Avatar::Avatar( std::string account )
    : m_account( account )
    , m_color( "bule" ) {
    // // 普攻无限次
    // m_item_map[ 0 ] = -1;
}
Avatar::~Avatar() {
    LOG( INFO ) << "~Avatar()" << m_account;
}

AvatarPtr Avatar::Create( std::string account ) {
    return AvatarPtr( new Avatar( account ) );
}

void Avatar::SetColor( std::string color ) {
    // std::lock_guard< std::mutex > lock( m_mutex );
    m_color = color;
}

void Avatar::AddAction( ActionType action_type, int times ) {
    // std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_item_map.find( action_type );
    if ( it == m_item_map.end() ) {
        m_item_map[ action_type ] = times;
    }
    else {
        it->second += times;
    }
}

bool Avatar::CheckAction( BaseActionPtr action ) {
    // std::lock_guard< std::mutex > lock( m_mutex );

    LOG( INFO ) << "check action:" << action->getName();
    for ( auto it : m_item_map ) {
        LOG( INFO ) << "key:" << static_cast<int>(it.first) << " value:" << it.second;
    }
    auto it = m_item_map.find( action->getActionType() );
    if ( it == m_item_map.end() ) {
        return false;
    }
    else {
        LOG( INFO ) << "action:" << action->getName() << " nums:" << it->second;
        if ( it->second == -1 ) {
            return true;
        }
        else if ( it->second > 0 ) {
            return true;
        }
        else {
            return false;
        }
    }
}

StandResponse Avatar::UseAction( BaseActionPtr action ) {
    // std::lock_guard< std::mutex > lock( m_mutex );

    auto it = m_item_map.find( action->getActionType() );
    if ( it == m_item_map.end() ) {
        return StandResponse( false, "don't have action " + action->getName() );
    }
    if ( it->second > 0 ) {
        --it->second;
        StandResponse res = action->doAction();
        if (res.m_ok) {
            return res;
        }
        else {
            // 动作执行失败要把次数加回去
            ++it->second;
        }
        return action->doAction();
    }
    else if ( it->second == -1 ) {
        return action->doAction();
    }
    return StandResponse( false, "don't have action " + action->getName() );
}

const std::string& Avatar::Account() const {
    return m_account;
}
const std::string& Avatar::Color() const {
    return m_color;
}

const std::map< ActionType, int >& Avatar::ItemMap() const {
    return m_item_map;
}
