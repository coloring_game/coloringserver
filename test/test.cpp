#include "Args.h"
#include "BaseGame.h"
#include "Timer.h"
#include "TurnBasedGame.h"
#include "../lib/SmartPtr.h"
#include <gtest/gtest.h>
#include <memory>
#include <numeric>
#include <vector>

// 测试集为 MyTest，测试案例为 Sum
TEST( MyTest, SmartPtr ) {
    SmartPtr< int >        p1( nullptr );
    std::shared_ptr< int > sp1( nullptr );
    EXPECT_EQ( p1.use_count(), sp1.use_count() );

    SmartPtr< int >        p2( p1 );
    std::shared_ptr< int > sp2( sp1 );
    EXPECT_EQ( p1.use_count(), sp1.use_count() );
    EXPECT_EQ( p2.use_count(), sp2.use_count() );

    SmartPtr< int >        p3( new int( 1 ) );
    std::shared_ptr< int > sp3( new int( 1 ) );
    EXPECT_EQ( p3.use_count(), sp3.use_count() );

    p1  = p3;
    sp1 = sp3;
    EXPECT_EQ( p1.use_count(), sp1.use_count() );
    EXPECT_EQ( p2.use_count(), sp2.use_count() );
    EXPECT_EQ( p3.use_count(), sp3.use_count() );
}

TEST( MyTest, GameLogic ) {
    TurnBasedGamePtr gamePtr = TurnBasedGame::Create();

    EXPECT_EQ( gamePtr->getStatus(), GameStatus::INIT );
    Args args;
    args.SetIntArgs( "m_max_player_num", 2 );
    args.SetIntArgs( "m_max_round_times", 10 );
    args.SetIntArgs( "m_max_turn_times", 2 );
    args.SetFloatArgs( "m_max_grid_rate", 0.65 );
    args.SetIntArgs( "m_is_single_game", 0 );
    gamePtr->paramsInit( args );
    EXPECT_EQ( gamePtr->getStatus(), GameStatus::PREPARE );
    gamePtr->addPlayer( "player1" );
    gamePtr->addPlayer( "player2" );
    EXPECT_EQ( gamePtr->getStatus(), GameStatus::RUNNING );
    args.SetIntArgs( "q", 2 );
    args.SetIntArgs( "r", 2 );
    args.SetIntArgs( "direction", 2 );
    EXPECT_EQ( gamePtr->playerAction( "player2", 1, args ).m_ok, false );
    EXPECT_EQ( gamePtr->playerAction( "player1", 1, args ).m_ok, true );
    args.SetIntArgs( "q", 0 );
    args.SetIntArgs( "r", 0 );
    EXPECT_EQ( gamePtr->playerAction( "player2", 1, args ).m_ok, true );

    // EXPECT_EQ( gamePtr->playerAction( "player1", 5, args ).m_ok, false );
    // EXPECT_EQ( gamePtr->playerAction( "player1", 2, args ).m_ok, false );
    // gamePtr->getPlayer( "player1" )->AddAction( ActionType::ArrowAction );
    // args.SetIntArgs( "q", 2 );
    // args.SetIntArgs( "r", 1 );
    // args.SetIntArgs( "direction", 5 );
    // EXPECT_EQ( gamePtr->playerAction( "player1", 2, args ).m_ok, true );
    // args.SetIntArgs( "q", 1 );
    // args.SetIntArgs( "r", 0 );
    // gamePtr->getPlayer( "player2" )->AddAction( ActionType::BlockAction );
    // EXPECT_EQ( gamePtr->playerAction( "player2", 3, args ).m_ok, true );
    // game.addPlayer( "1" );
    // TurnBasedGame* tbg_ptr = new TurnBasedGame();
    // EXPECT_EQ( tbg_ptr->Status(), GameStatus::INIT );
    // tbg_ptr->init();
    // EXPECT_EQ( tbg_ptr->Status(), GameStatus::PREPARE );
    // tbg_ptr->addPlayer( "2" );
    // EXPECT_EQ( tbg_ptr->Status(), GameStatus::RUNNING );
    // tbg_ptr->EndGame();
    // EXPECT_EQ( tbg_ptr->Status(), GameStatus::END );
}

int main( int argc, char* argv[] ) {
    ::testing::InitGoogleTest( &argc, argv );
    google::InitGoogleLogging( argv[ 0 ] );
    FLAGS_log_dir = "../log";
    LOG( INFO ) << "InitGoogleLogging ";
    return RUN_ALL_TESTS();
}