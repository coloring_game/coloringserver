**Coloring Demo 1**

## **1.简介**

这是一个多人的回合制棋盘游戏，玩家每回合行动一次，通过不同的行动对棋盘进行染色，若己方色块完全包围了一篇区域就可以将区域内的色块全部染色，10回合后占领的格子最多的玩家获得胜利。

## **2.游戏规则**

### **回合制：**

一共10回合，每个玩家每回合只能行动一次，即只可以选择一种行动方式来行动

### **可选的四种行动方式：**

0 Attack(普攻)  1 Arrow(箭)  2 Hexagon(六边形) 3 Triangle(三角形)

### **行动起始点限制：**

- 第一回合，行动起始点必须是空白点

- 其余回合，行动起始点必须与己方色块相邻

### **行动方式次数限制：**

- 0 Attack(普攻) 默认无数次

- 1 Arrow(箭)  2 Hexagon(六边形) 3 Triangle(三角形) 默认0次

### **发放行动方式：**

- 在第6和第9回合会分别发放1个 Arrow(箭)

- 在第2回合会发放1个Hexagon(六边形) 

- 在第3回合会发放2个Triangle(三角形)

### **包围规则：**

若己方色块完全包围了一篇区域就可以将区域内的色块全部染色

### **结算规则：**

10回合结束时谁颜色多谁赢

## **3.游玩步骤**

### **3.1登录**

需要用一个不会重名的用户名，建议是姓名拼音，然后密码无所谓，直接登录就行了

### **3.2创建房间**

- 点击 CreateRoom 创建房间

- 或者先输入Room_id，再点击EnterRoom，进入一个已经创建好的房间

### **3.3正式进入游戏**

![img](https://xix4vmasr7.feishu.cn/space/api/box/stream/download/asynccode/?code=MzYyMTE1ZjQ3M2MwZDQ4OWU0YjNmNjY0NDlkOTg5ZjJfcm0yMnJtYTZMTWJyOGU4dk1sVDRzektuYk1RanFQb1BfVG9rZW46Ym94Y25SN3ZkMTYzR3dnVGRxRnpIekZZd1JnXzE2NDk2OTI5NzQ6MTY0OTY5NjU3NF9WNA)



### **3.4退出游戏**

alt + F4 (😥)

## 4.TODO

- 道具种类多样化，道具几何形状的设计（不一定要规则几何图形【俄罗斯方块】）
- 回合制的每回合时间（急速模式【每回合时间设置的很短】）
- 完善善后内容（退出登录、退出房间）
- 完善持久化内容（数据库）
- Arrow削弱

## **5.建议（游戏规则的完善、修改、以及其他任何你想说的）**

多多益善

- 大地图形状不一定要是六边形，可以是其他形状（两边长中间宽）
- 形成包围后发放奖励道具
- 吃棋后的效果（吃棋翻转）
- 下完以后不知道是怎么下的（回放）

## **6.Bug**

- 获取不到client？

![img](https://xix4vmasr7.feishu.cn/space/api/box/stream/download/asynccode/?code=N2M4ZGMxZDZmZWYyZTI4NjY3NTFhZDkyNGZjZjg5MThfenQ2M1ZMTEg5ZkNSeHZkSmZwNjVoUGlRWmJpaUlIcWZfVG9rZW46Ym94Y25BbThVSlNuYjRrQW5HTzlmNkxBM0V1XzE2NDk2OTI5NzQ6MTY0OTY5NjU3NF9WNA)

- 第二回合直接覆盖

![img](https://xix4vmasr7.feishu.cn/space/api/box/stream/download/asynccode/?code=NDNhOTgxMjFlMDUxOGU5ZDY1OGQ3MzQ1MTc3Y2E4NjRfZnZlNVJIQW9BcTNFM3NtOVRmZUdOTlhvenRPcm0zbDVfVG9rZW46Ym94Y25wTlpYMW5MOGpDM21zUlh2cjRKNXhlXzE2NDk2OTI5NzQ6MTY0OTY5NjU3NF9WNA)