#ifndef REFLECT_H_
#define REFLECT_H_

#include <map>

template < typename NameClass, typename Factory > class Reflector {
private:
    Reflector() {
        ;
    }
    ~Reflector() {
        std::cout << "~Reflector()" << std::endl;
        for ( auto it = factorys.begin(); it != factorys.end(); ++it ) {
            if ( it->second != nullptr ) {
                delete it->second;
            }
        }
        factorys.clear();
    }

public:
    void registerFactory( const NameClass& name, Factory* f ) {
        auto it = factorys.find( name );
        if ( it != factorys.end() ) {
            throw "same factory name";
        }
        factorys[ name ] = f;
    }
    Factory* getFactory( const NameClass& name ) {
        auto it = factorys.find( name );
        if ( it != factorys.end() ) {
            return it->second;
        }
        return nullptr;
    }
    static Reflector& getReflector() {
        static Reflector instance;
        return instance;
    }

private:
    std::map< NameClass, Factory* > factorys;
};

#endif  // REFLECT_H_