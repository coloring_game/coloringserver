#ifndef SMARTPTR_H_
#define SMARTPTR_H_
#include <iostream>
#include <cassert>
template < typename T > class SmartPtr {
public:
    inline SmartPtr( T* ptr = nullptr );

    inline ~SmartPtr();

    inline SmartPtr( const SmartPtr& ptr );

    inline SmartPtr& operator=( const SmartPtr& ptr );

    T& operator*() {
        assert( m_ptr != nullptr );
        return *m_ptr;
    }

    T* operator->() {
        assert( m_ptr != nullptr );
        return m_ptr;
    }
    friend bool operator==( SmartPtr* lhs, SmartPtr& rhs ) {
        if ( lhs == nullptr ) {
            return nullptr == rhs.m_ptr;
        }
        return lhs->m_ptr == rhs.m_ptr;
    }
    friend bool operator==( SmartPtr& lhs, SmartPtr* rhs ) {
        if ( rhs == nullptr ) {
            return lhs.m_ptr == nullptr;
        }
        return lhs.m_ptr == rhs->m_ptr;
    }
    friend bool operator==( SmartPtr& lhs, SmartPtr& rhs ) {
        return lhs.m_ptr == rhs.m_ptr;
    }
    friend bool operator==( SmartPtr&& lhs, SmartPtr& rhs ) {
        return lhs.m_ptr == rhs.m_ptr;
    }
    friend bool operator==( SmartPtr& lhs, SmartPtr&& rhs ) {
        return lhs.m_ptr == rhs.m_ptr;
    }
    friend bool operator==( SmartPtr&& lhs, SmartPtr* rhs ) {
        if ( rhs == nullptr ) {
            return lhs.m_ptr == nullptr;
        }
        return lhs.m_ptr == rhs->m_ptr;
    }
    friend bool operator==( SmartPtr* lhs, SmartPtr&& rhs ) {
        if ( lhs == nullptr ) {
            return nullptr == rhs.m_ptr;
        }
        return lhs->m_ptr == rhs.m_ptr;
    }
    friend bool operator==( SmartPtr&& lhs, SmartPtr&& rhs ) {
        return lhs.m_ptr == rhs.m_ptr;
    }

    const int use_count() {
        return getCount();
    }
    const int getCount() {
        return *m_count;
    }

private:
    void addCount() {
        ++( *m_count );
    }
    void subCount() {
        --( *m_count );
    }

    T*   m_ptr;
    int* m_count;
};

template < typename T >
SmartPtr< T >::SmartPtr( T* ptr )
    : m_ptr( ptr )
    , m_count( new int( 0 ) ) {

    if ( ptr != nullptr ) {
        addCount();
    }
}

template < typename T > SmartPtr< T >::~SmartPtr() {

    if ( m_ptr == nullptr ) {
        // delete m_count;
    }
    else {
        // std::cout << typeid( *m_ptr ).name() << " ~ --count" << std::endl;
        subCount();
        if ( getCount() == 0 ) {
            delete m_ptr;
            delete m_count;
        }
    }
}

template < typename T > SmartPtr< T >::SmartPtr( const SmartPtr& ptr ) {

    // 引发了少调用拷贝构造的问题 if ( m_ptr != ptr.m_ptr ) {
    m_ptr   = ptr.m_ptr;
    m_count = ptr.m_count;
    if ( m_ptr != nullptr ) {
        // std::cout << typeid( *m_ptr ).name() << "copy ++count" << std::endl;
        addCount();
    }
}

template < typename T > SmartPtr< T >& SmartPtr< T >::operator=( const SmartPtr& ptr ) {

    if ( m_ptr == ptr.m_ptr ) {
        return *this;
    }
    if ( m_ptr != nullptr ) {
        // std::cout << typeid( *m_ptr ).name() << "operator= --count" << std::endl;
        subCount();
        if ( getCount() == 0 ) {
            delete m_ptr;
            delete m_count;
        }
    }
    m_ptr   = ptr.m_ptr;
    m_count = ptr.m_count;
    // std::cout << typeid( *m_ptr ).name() << "operator= ++count" << std::endl;
    addCount();
    return *this;
}
#endif  // SMARTPTR_H_