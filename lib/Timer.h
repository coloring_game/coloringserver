#ifndef TIMER_H_
#define TIMER_H_

#include <functional>
#include <iostream>
#include <mutex>
#include <queue>
#include <sys/time.h>

class Timer {
public:
    explicit Timer( unsigned long long expired_time, std::function< void( void ) > func );

    void run();

    unsigned long long get_expired_time();

private:
    int                           m_expired_time;
    std::function< void( void ) > m_func;
};

class TimerManager {
public:
    explicit TimerManager();

    Timer* add_timer( int time_out, std::function< void( void ) > func );

    void add_timer( Timer*& timer );

    void delete_timer( Timer*& timer );

    void pop_timer();

    void tick();

private:
    struct compare {
        bool operator()( Timer*& t1, Timer*& t2 ) {
            return t1->get_expired_time() > t2->get_expired_time();
        }
    };
    unsigned long long get_now_millisecond();

    // 最小堆
    std::priority_queue< Timer*, std::vector< Timer* >, compare > m_queue;

    std::mutex m_mutex;
};

#endif  // TIMER_H_