#include "Timer.h"
// 单位是10-3 s
Timer::Timer( unsigned long long expired_time, std::function< void( void ) > func )
    : m_expired_time( expired_time )
    , m_func( func ) {
    ;
}

void Timer::run() {
    m_func();
}

unsigned long long Timer::get_expired_time() {
    return m_expired_time;
}

TimerManager::TimerManager() {
    ;
}

Timer* TimerManager::add_timer( int time_out, std::function< void( void ) > func ) {
    unsigned long long now_time = get_now_millisecond();
    Timer*             timer    = new Timer( now_time + time_out, func );

    std::lock_guard< std::mutex > lock( m_mutex );
    m_queue.push( timer );
    return timer;
}

void TimerManager::add_timer( Timer*& timer ) {
    if ( timer == nullptr ) {
        return;
    }
    std::lock_guard< std::mutex > lock( m_mutex );
    m_queue.push( timer );
}

void TimerManager::delete_timer( Timer*& timer ) {
    // LOG( INFO ) << "delete_timer out" ;
    if ( timer == nullptr ) {
        // LOG( INFO ) << "delete_timer nullptr" ;
        return;
    }
    std::lock_guard< std::mutex > lock( m_mutex );
    // LOG( INFO ) << "delete_timer lock" ;
    if ( !m_queue.empty() ) {
        // LOG( INFO ) << "delete_timer if" ;
        std::priority_queue< Timer*, std::vector< Timer* >, compare > new_queue;
        // LOG( INFO ) << "delete_timer new_queue" ;
        while ( !m_queue.empty() ) {
            // LOG( INFO ) << "delete_timer while" ;
            Timer* top = m_queue.top();
            m_queue.pop();
            if ( top != timer ) {
                new_queue.push( top );
            }
            else {
                delete top;
            }
        }
        m_queue = new_queue;
    }
}

void TimerManager::pop_timer() {
    std::lock_guard< std::mutex > lock( m_mutex );
    m_queue.pop();
}

void TimerManager::tick() {
    unsigned long long now_time = get_now_millisecond();

    while ( !m_queue.empty() ) {
        Timer* top = m_queue.top();
        // LOG( INFO ) << top->get_expired_time() << now_time ;
        if ( top->get_expired_time() <= now_time ) {
            pop_timer();
            top->run();

            // 这里delete
            delete top;

            continue;
        }
        break;
    }
}

unsigned long long TimerManager::get_now_millisecond() {
    struct timespec ts;
    clock_gettime( CLOCK_MONOTONIC_RAW, &ts );
    return ts.tv_sec * 1000 + ts.tv_nsec / ( 1000 * 1000 );
}
