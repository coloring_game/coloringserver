#include <iostream>
#include <mysql++.h>
#include <string>

using namespace std;
using namespace mysqlpp;

int main( int argc, char** argv ) {
    Connection conn( false );
    conn.connect( "coloring_db", "127.0.0.1", "root", "" );
    if ( !conn ) {
        cerr << "ERROR: " << conn.error() << endl;
        return -1;
    }else{
        cout << "Connected to database." << endl;
    }
    Query query = conn.query( "insert into friends (name,email) values( %0q,%1q)" );
    query.parse();
    query.execute( "mengguang", "mengguang@gmail.com" );
    cout << "ID: " << query.insert_id() << endl;
    query.execute( "mengkang", "mengkang@163.com" );
    cout << "ID: " << query.insert_id() << endl;

    query.reset();
    query << "select * from friends";
    StoreQueryResult res = query.store();
    int              id;
    string           name;
    string           email;
    for ( auto row : res ) {
        for ( auto item : row ) {
            cout << item << " ";
        }
        cout << endl;

        for ( unsigned int i = 0; i < row.size(); i++ ) {
            cout << row[ i ] << " ";
        }
        cout << endl;

        cout << row[ "id" ] << " " << row[ "name" ] << " " << row[ "email" ] << endl;

        id = row[ "id" ];
        row[ "name" ].to_string( name );
        row[ "email" ].to_string( email );
        cout << id << " " << name << " " << email << endl;
    }
    query.reset();
    query << "delete from friends";
    query.execute();
    cout << "rows deleted: " << query.affected_rows() << endl;
    return 0;
}