#ifndef WEB_SOCKET_SERVICE_H_
#define WEB_SOCKET_SERVICE_H_
#include "main.h"
#include "Args.h"
#include "BaseService.h"
#include "StandResponse.h"
#include "nlohmann/json.hpp"
#include <glog/logging.h>
using namespace cinatra;
using json = nlohmann::json;
class WebSocketService : public BaseService {
public:
    void GetFunc( request& req, response& res ) override;
};

void WebSocketService::GetFunc( request& req, response& res ) {
    std::cout << req.get_method() << std::endl;
    if ( req.get_content_type() != content_type::websocket ) {
        return;
    }

    req.on( ws_open, []( request& req ) { std::cout << "websocket start" << std::endl; } );

    req.on( ws_message, []( request& req ) {
        auto        part_data = req.get_part_data();
        std::string account   = string_vw_to_string( part_data );
        // std::move 把account的转移了！@_@
        // req.get_conn< cinatra::NonSSL >()->send_ws_string( std::move( account ) );
        std::cout << part_data.data() << std::endl;
        ClientObject* co = client_map.get( account, false );
        if ( co != nullptr ) {
            co->SetConnection( req );
        }
        req.get_conn< cinatra::NonSSL >()->send_ws_string( std::move( account ) );
    } );

    req.on( ws_error, []( request& req ) {
        std::cout << "websocket pack error or network error" << std::endl;
    } );
    req.on( ws_close, []( request& req ) {
        std::cout << "websocket close" << std::endl;
    } );
}

#endif  // WEB_SOCKET_SERVICE_H_