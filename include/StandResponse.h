#ifndef STAND_RESPONSE_H_
#define STAND_RESPONSE_H_
#include <glog/logging.h>
#include <string>
#include <iostream>
class StandResponse {
public:
    bool        m_ok;
    std::string m_msg;

    StandResponse( bool ok, std::string msg );
};
#endif  // STAND_RESPONSE_H_