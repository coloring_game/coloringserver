#ifndef ARGS_H_
#define ARGS_H_
#include <map>
#include <string>
// TODO Args应改为引用传入
class Args {
public:
    void        SetStringArgs( std::string key, std::string value );
    std::string GetStringArgs( std::string key );

    int  GetIntArgs( std::string key );
    void SetIntArgs( std::string key, int value );

    float GetFloatArgs( std::string key );
    void  SetFloatArgs( std::string key, float value );

    std::map< std::string, std::string > string_args_map;
    std::map< std::string, int >         int_args_map;
    std::map< std::string, float >       float_args_map;
};

#endif  // ARGS_H_