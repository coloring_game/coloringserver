#ifndef START_HEX_ACTION_H_
#define START_HEX_ACTION_H_

#include "BaseAction.h"

// 拥有起点的动作
class StartHexAction : public BaseAction, public CheckStartHexInterface {
public:
    StartHexAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args );
    virtual ~StartHexAction();

    virtual StandResponse doAction();

    virtual const std::string getName() = 0;

    virtual const ActionType getActionType() = 0;

    Hexagonal::Hex getStartHex() {
        return m_start_hex;
    }
    int getDirection() {
        return m_direction;
    }
    bool isNeedCheckStartHex() {
        return getArgs().GetIntArgs( "current_turn" ) > getArgs().GetIntArgs( "check_starthex_turn" );
    }

private:
    Hexagonal::Hex m_start_hex;
    int            m_direction;
};

#endif  // START_HEX_ACTION_H_