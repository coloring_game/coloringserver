#ifndef ROOM_SERVICE_H_
#define ROOM_SERVICE_H_
#include "BaseService.h"
#include "RoomMap.h"
#include "StandResponse.h"
#include "nlohmann/json.hpp"
#include <glog/logging.h>
using namespace cinatra;
using json = nlohmann::json;
extern ClientMap      client_map;
extern GameManagerMap room_map;
class RoomService : public BaseService {
    // CreateRoom
    void PostFunc( request& req, response& res ) override;
    // EnterRoom
    void PutFunc( request& req, response& res ) override;
    // CloseRoom
    void DeleteFunc( request& req, response& res ) override;
    // ListRoom && DetailRoom(GetMap)
    void GetFunc( request& req, response& res ) override;
};
void RoomService::PostFunc( request& req, response& res ) {
    LOG( INFO ) << "RoomService::PostFunc";
    std::string   account = string_vw_to_string( req.get_header_value( "token" ) );
    ClientObject* co      = client_map.get( account );
    if ( co == nullptr ) {
        LOG( INFO ) << "Not find client object";
        res.set_status_and_content( status_type::not_found );
        return;
    }
    int room_type = stoi( string_vw_to_string( req.get_query_value( "room_type" ) ) );
    int room_id   = room_map.CreateRoom( room_type, 2 );
    // 创建房间的玩家创建成功后自动进入房间
    bool ok = room_map.EnterRoom( room_id, *co );
    if ( !ok ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    TurnBasedGamePtr gm = room_map.GetRoomPtr( room_id );
    if ( gm == nullptr ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    auto it = gm->getPlayer( co->Account() );
    json data;
    data[ "room_id" ] = room_id;
    data[ "color" ]   = it->Color();
    std::cout << room_id << std::endl;
    res.set_status_and_content( status_type::ok, data.dump(), JSON );
    client_map.sendMsg( "update_room_list" );
}
void RoomService::PutFunc( request& req, response& res ) {
    LOG( INFO ) << "RoomService::PutFunc";
    std::string   account = string_vw_to_string( req.get_header_value( "token" ) );
    ClientObject* co      = client_map.get( account );
    if ( co == nullptr ) {
        LOG( INFO ) << "Not find client object";
        res.set_status_and_content( status_type::not_found );
        return;
    }
    // TODO 需要加上对room_id的检验
    int  room_id = stoi( string_vw_to_string( req.get_query_value( "room_id" ) ) );
    bool ok      = room_map.EnterRoom( room_id, *co );
    if ( !ok ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    TurnBasedGamePtr gm = room_map.GetRoomPtr( room_id );
    if ( gm == nullptr ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    auto it = gm->getPlayer( co->Account() );
    json data;
    data[ "color" ] = it->Color();
    res.set_status_and_content( status_type::ok, data.dump(), JSON );
    client_map.sendMsg( "update_room_list" );
    for ( auto it : gm->getPlayerMap() ) {
        ClientObject* co = client_map.get( it.first );
        if ( co != nullptr ) {
            co->SendMessage( "update_room_detail" );
        }
    }
}
void RoomService::DeleteFunc( request& req, response& res ) {
    LOG( INFO ) << "RoomService::DeleteFunc";
    // TODO 需要加上房主能否关闭房间的判断
    // TODO 需要加上对room_id的检验
    int  room_id = stoi( string_vw_to_string( req.get_query_value( "room_id" ) ) );
    bool ok      = room_map.CloseRoom( room_id );
    if ( !ok ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    res.set_status_and_content( status_type::ok );
    client_map.sendMsg( "update_room_list" );
}
void RoomService::GetFunc( request& req, response& res ) {
    LOG( INFO ) << "RoomService::GetFunc";
    std::string   account = string_vw_to_string( req.get_header_value( "token" ) );
    ClientObject* co      = client_map.get( account );
    if ( co == nullptr ) {
        LOG( INFO ) << "Not find client object";
        res.set_status_and_content( status_type::not_found );
        return;
    }
    json data;
    if ( string_vw_to_string( req.get_query_value( "room_id" ) ).size() > 0 ) {
        // DetailRoom
        int room_id = stoi( string_vw_to_string( req.get_query_value( "room_id" ) ) );

        TurnBasedGamePtr gm = room_map.GetRoomPtr( room_id );

        if ( gm == nullptr ) {
            res.set_status_and_content( status_type::not_found );
            return;
        }
        data[ "player_info" ] = {};
        for ( auto& player : gm->getPlayerMap() ) {
            data[ "player_info" ][ player.first ] = {};
            for ( auto it : player.second->ItemMap() ) {
                data[ "player_info" ][ player.first ][ std::to_string( static_cast< int >( it.first ) ) ] = it.second;
            }
        }
        data[ "now_round" ]  = gm->nowRound();
        data[ "now_player" ] = gm->nowPlayer()->Account();

        data[ "map_info" ] = {};

        Hexagonal::HexGridMapPtr hexgrid_map = gm->getMap();

        for ( auto it : hexgrid_map->GetMap() ) {
            for ( auto my_grid : it ) {
                if ( my_grid != nullptr ) {
                    std::string key = my_grid->getStrHex();

                    data[ "map_info" ][ key ]                     = {};
                    data[ "map_info" ][ key ][ "q" ]              = my_grid->m_pos.q;
                    data[ "map_info" ][ key ][ "r" ]              = my_grid->m_pos.r;
                    data[ "map_info" ][ key ][ "is_need_border" ] = my_grid->IsNeedBorder();
                    data[ "map_info" ][ key ][ "color" ]          = my_grid->m_color;
                }
            }
        }
    }
    else {
        // ListRoom
        for ( auto it : room_map.m_map ) {
            int room_id                       = it.first;
            data[ std::to_string( room_id ) ] = {};
            TurnBasedGamePtr gm               = it.second;
            for ( auto& player : gm->getPlayerMap() ) {
                data[ std::to_string( room_id ) ][ player.first ] = true;
            }
        }
    }
    res.set_status_and_content( status_type::ok, data.dump(), JSON );
}
#endif  // ROOM_SERVICE_H_