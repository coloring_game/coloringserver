#ifndef MIAN_H_
#define MIAN_H_

#include <glog/logging.h>
#include <iostream>
class ClientMap;
class TimerManager;
class GameManagerMap;
extern ClientMap      client_map;
extern TimerManager   timer_manager;
extern GameManagerMap room_map;
// extern HexGridStorage hex_grid_storage;

#endif  // MIAN_H_