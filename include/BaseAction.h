#ifndef BASE_ACTION_H_
#define BASE_ACTION_H_

#include "Args.h"
#include "Hexagonal.h"
#include "StandResponse.h"
#include "Reflect.h"
#include <string>

#define REGISTER_BASEACTION( name )                                                                            \
    class Factory_##name : public BaseActionFactory {                                                          \
    public:                                                                                                    \
        virtual BaseAction* create( Hexagonal::HexGridMapPtr hexgrid_map, Args args ) override {               \
            std::cout << "print()" << std::endl;                                                               \
            BaseAction* p = new name( hexgrid_map, args );                                                     \
            return p;                                                                                          \
        }                                                                                                      \
        virtual void print() {                                                                                 \
            std::cout << "print()" << std::endl;                                                               \
        }                                                                                                      \
    };                                                                                                         \
    class Register_##name {                                                                                    \
    public:                                                                                                    \
        Register_##name() {                                                                                    \
            BaseActionFactory* p = new Factory_##name();                                                       \
            Reflector< ActionType, BaseActionFactory >::getReflector().registerFactory( ActionType::name, p ); \
        }                                                                                                      \
    };                                                                                                         \
    Register_##name register_##name;

// 枚举类型用于客户端参数映射
enum class ActionType : int { BaseAction = 0, Attack = 1, ArrowAction = 2, BlockAction = 3 };
class BaseAction {
public:
    BaseAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args );

    virtual ~BaseAction();

    virtual StandResponse doAction()   = 0;
    virtual StandResponse onDoAction() = 0;

    Args& getArgs();

    Hexagonal::HexGridMapPtr getMap();

    virtual const std::string getName()       = 0;
    virtual const ActionType  getActionType() = 0;

    std::string getColor() {
        return m_color;
    }

private:
    Args m_args;

    Hexagonal::HexGridMapPtr m_hexgrid_map;

    std::string m_color;
};

class BaseActionFactory {
public:
    BaseActionFactory() {
        std::cout << "BaseActionFactory()" << std::endl;
    }
    virtual ~BaseActionFactory() {
        std::cout << "~BaseActionFactory()" << std::endl;
    }
    virtual void print() {
        std::cout << "BaseActionFactory::print()" << std::endl;
    }
    virtual BaseAction* create( Hexagonal::HexGridMapPtr hexgrid_map, Args args ) = 0;
};

class CheckStartHexInterface {
public:
    virtual StandResponse checkStartHex( Hexagonal::Hex start_hex, Hexagonal::HexGridMapPtr hexgrid_map, std::string color );
};

#endif  // BASE_ACTION_H_