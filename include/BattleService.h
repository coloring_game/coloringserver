#ifndef BATTLE_SERVICE_H_
#define BATTLE_SERVICE_H_

#include "Args.h"
#include "BaseService.h"
#include "RoomMap.h"
#include "StandResponse.h"
#include "nlohmann/json.hpp"
#include <glog/logging.h>
using namespace cinatra;
using json = nlohmann::json;
extern ClientMap      client_map;
extern GameManagerMap room_map;
class BattleService : public BaseService {
public:
    void PostFunc( request& req, response& res ) override;
};

void BattleService::PostFunc( request& req, response& res ) {
    std::string   account = string_vw_to_string( req.get_header_value( "token" ) );
    ClientObject* co      = client_map.get( account );
    if ( co == nullptr ) {
        LOG( INFO ) << "Not find client object";
        res.set_status_and_content( status_type::not_found );
        return;
    }
    // TODO 需要加上对room_id的检验
    int              room_id = stoi( string_vw_to_string( req.get_query_value( "room_id" ) ) );
    TurnBasedGamePtr gm      = room_map.GetRoomPtr( room_id );
    if ( gm == nullptr ) {
        LOG( INFO ) << "Not find room";
        res.set_status_and_content( status_type::not_found );
        return;
    }
    int  action_type = stoi( string_vw_to_string( req.get_query_value( "action_type" ) ) );
    int  direction   = stoi( string_vw_to_string( req.get_query_value( "direction" ) ) );
    int  q           = stoi( string_vw_to_string( req.get_query_value( "q" ) ) );
    int  r           = stoi( string_vw_to_string( req.get_query_value( "r" ) ) );
    Args args;
    args.SetIntArgs( "q", q );
    args.SetIntArgs( "r", r );
    args.SetIntArgs( "direction", direction );
    StandResponse response = gm->playerAction( co->Account(), action_type, args );
    // 执行成功了需要通知房间里的所有客户端更新
    if ( response.m_ok ) {
        for ( auto it : gm->getPlayerMap() ) {
            ClientObject* co = client_map.get( it.first );
            if ( co != nullptr ) {
                co->SendMessage( "update_room_detail" );
            }
        }
    }
    json data;
    data[ "is_ok" ] = response.m_ok;
    data[ "msg" ]   = response.m_msg;
    res.set_status_and_content( status_type::ok, data.dump(), JSON );
}

#endif  // BATTLE_SERVICE_H_