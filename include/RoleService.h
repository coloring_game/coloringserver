#ifndef ROLE_SERVICE_H_
#define ROLE_SERVICE_H_

#include "BaseService.h"
#include "nlohmann/json.hpp"
#include <glog/logging.h>
using namespace cinatra;
using json = nlohmann::json;
extern ClientMap client_map;
void             ContinueOnline( ClientObject* co, std::string account );
class RoleService : public BaseService {
public:
    // 角色登陆或注册
    void PostFunc( request& req, response& res ) override;

    // 心跳
    void GetFunc( request& req, response& res ) override;
};
void ContinueOnline( ClientObject* co, std::string account ) {
    if ( co == nullptr ) {
        return;
    }
    // 登录成功后要重设timer
    LOG( INFO ) << "delete_timer";
    timer_manager.delete_timer( co->GetTimer() );
    auto func = std::bind( &ClientMap::remove, &client_map, account );
    LOG( INFO ) << "add_timer";
    // Timer* timer = timer_manager.add_timer( 5000 * 1000, func );
    LOG( INFO ) << "set_timer";
    // co->SetTimer( timer );
}
// 角色登陆或注册
void RoleService::PostFunc( request& req, response& res ) {
    LOG( INFO ) << "RoleService::POST";
    std::string account = string_vw_to_string( req.get_query_value( "account" ) );
    // std::string account{ account_view.data(), account_view.size() };
    json data;
    data[ "token" ] = account;
    // 判断是否存在Role
    ClientObject* co = client_map.get( account );
    if ( co != nullptr ) {
        LOG( INFO ) << "already have role";
        res.set_status_and_content( status_type::ok, data.dump(), JSON );
    }
    else {
        co = new ClientObject( account );
        client_map.set( account, co );
        LOG( INFO ) << "set Role";
        res.set_status_and_content( status_type::ok, data.dump(), JSON );
    }
    ContinueOnline( co, account );
};

// 心跳
void RoleService::GetFunc( request& req, response& res ) {
    // heartbeat要删除原有timer，并插入新timer
    std::string account = string_vw_to_string( req.get_header_value( "token" ) );
    if ( account.size() == 0 ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }

    ClientObject* co = client_map.get( account );

    if ( co == nullptr ) {
        res.set_status_and_content( status_type::not_found );
        return;
    }
    ContinueOnline( co, account );

    res.set_status_and_content( status_type::ok );
}
#endif  // ROLE_SERVICE_H_