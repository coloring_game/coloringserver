#ifndef TURN_BASED_GAME_H_
#define TURN_BASED_GAME_H_

#include "BaseGame.h"
#include "AI.h"
class TurnBasedGame;
using TurnBasedGamePtr = SmartPtr< TurnBasedGame >;
class TurnBasedGame : public BaseGame {
public:
    TurnBasedGame();
    ~TurnBasedGame();

    static TurnBasedGamePtr Create() {
        return TurnBasedGamePtr( new TurnBasedGame() );
    }

    virtual void paramsInit( Args& args ) override;

    virtual StandResponse onAddPlayer( AvatarPtr avatar ) override;

    // 该回合玩家行动完毕
    virtual void nextPlayer();

    // 获得该回合正在行动的玩家
    virtual AvatarPtr nowPlayer();

    // 想要行动的玩家是否是轮到他行动了
    virtual bool isNowPlayer( AvatarPtr avatar );

    virtual StandResponse onPlayerAction( AvatarPtr avatar, BaseActionPtr action ) override;

    virtual void onStartGame() override;

    // 每一回合的开始和结束
    void         StartRound();
    void         EndRound();
    virtual void onStartRound();
    virtual void onEndRound();

    virtual void onStartTurn() override;
    virtual void onEndTurn() override;

    // 行动结束判断, 是否可以进入下一个Round
    virtual StandResponse onCheckTurnFinish() override;

    // 回合结束判断, 是否需要结束游戏
    StandResponse         checkRoundFinish();
    virtual StandResponse onCheckRoundFinish();

    // 游戏结束判断, 是否达到了除回合数到了以外的其他游戏结束条件
    virtual StandResponse onCheckGameFinish() override;

    const int nowRound() {
        return m_current_round_times;
    }

    bool isSingleGame() {
        return m_is_single_game == 1;
    }

    AIPtr getAI() {
        return m_ai;
    }

private:
    void _ResetActiveQueue();
    // 最大回合数
    int m_max_round_times;
    int m_max_turn_times;
    // 每回合的最大行动次数，即玩家数
    int m_current_round_times;
    int m_current_turn_times;
    // 获胜阈值
    float m_max_grid_rate;
    // 是否是单人游戏
    int m_is_single_game;

    // 机器人AI指针
    AIPtr m_ai;

    std::queue< AvatarPtr > m_active_queue;
};

#endif  // TURN_BASED_GAME_H_