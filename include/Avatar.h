#ifndef AVATAR_H_
#define AVATAR_H_

#include "../lib/SmartPtr.h"
#include "ActionFactory.h"
#include "StandResponse.h"
#include <map>
#include <memory>  //shared_ptr
#include <mutex>
#include <string>

class Avatar;
using AvatarPtr = SmartPtr< Avatar >;
class Avatar {
public:
    Avatar( std::string account );

    ~Avatar();

    static AvatarPtr Create( std::string account );

    // bool operator==( Avatar avatar ) {
    //     return ( avatar.m_account == m_account );
    // }

    void SetColor( std::string color );

    void AddAction( ActionType action_type, int times = 1 );

    bool CheckAction( BaseActionPtr action );

    StandResponse UseAction( BaseActionPtr action );

    const std::string& Account() const;

    const std::string& Color() const;

    const std::map< ActionType, int >& ItemMap() const;

private:
    std::string m_account;
    std::string m_color;
    // std::mutex  m_mutex;

    std::map< ActionType, int > m_item_map;
};

#endif  // AVATAR_H_