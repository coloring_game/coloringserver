#ifndef BASE_SERVICE_H_
#define BASE_SERVICE_H_
#include "cinatra.hpp"
#include <glog/logging.h>
using namespace cinatra;

std::string string_vw_to_string( std::string_view s ) {
    return { s.data(), s.size() };
}
class BaseService {
public:
    BaseService();
    virtual ~BaseService();
    virtual void PostFunc( request& req, response& res );
    virtual void DeleteFunc( request& req, response& res );
    virtual void GetFunc( request& req, response& res );
    virtual void PutFunc( request& req, response& res );
    void         REST( request& req, response& res );
};

BaseService::BaseService() {
    LOG( INFO ) << "BaseService::BaseService";
};
BaseService::~BaseService() {
    ;
};
void BaseService::PostFunc( request& req, response& res ) {
    std::cout << "BaseService::POST" << std::endl;
    res.set_status( status_type::not_found );
};
void BaseService::DeleteFunc( request& req, response& res ) {
    std::cout << "BaseService::DELETE" << std::endl;
    res.set_status( status_type::not_found );
};
void BaseService::GetFunc( request& req, response& res ) {
    std::cout << "BaseService::GET" << std::endl;
    res.set_status( status_type::not_found );
};
void BaseService::PutFunc( request& req, response& res ) {
    std::cout << "BaseService::PUT" << std::endl;
    res.set_status( status_type::not_found );
};
void BaseService::REST( request& req, response& res ) {
    res.add_header( "Access-Control-Allow-Credentials", "true" );
    auto method_sv = req.get_method();
    if ( method_sv == "OPTIONS" ) {
        res.add_header( "Access-Control-Allow-Origin", "*" );
        res.add_header( "Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS" );
        res.add_header( "Access-Control-Allow-Headers", "*" );
        res.set_status_and_content( status_type::ok );
        return;
    }
    else {
        res.add_header( "Access-Control-Allow-Origin", string_vw_to_string( req.get_header_value( "Origin" ) ) );
        if ( method_sv == "POST" ) {
            PostFunc( req, res );
        }
        else if ( method_sv == "DELETE" ) {
            DeleteFunc( req, res );
        }
        else if ( method_sv == "GET" ) {
            GetFunc( req, res );
        }
        else if ( method_sv == "PUT" ) {
            PutFunc( req, res );
        }
        else {
            res.set_status_and_content( status_type::not_found );
        }
    }
}

#endif  // BASE_SERVICE_H_