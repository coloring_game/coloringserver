#ifndef BASE_GAME_H_
#define BASE_GAME_H_

#include "ActionFactory.h"
#include "Args.h"
#include "Avatar.h"
#include "Hexagonal.h"
#include "StandResponse.h"
#include <map>
#include <queue>
#include <string>
#include <vector>
/*
INIT    初始状态, 用于初始化 onCall 函数
PREPARE 准备状态, 用于等待角色加入游戏
RUNNING 运行状态, 用于游戏进行中
END     结束状态, 用于游戏结束
*/
enum class GameStatus : int { INIT, PREPARE, RUNNING, END };

class StandResponse;
class BaseGame;
using BaseGamePtr = SmartPtr< BaseGame >;
// BaseGame 类是游戏的基类, 它定义了游戏的基本功能, 包括游戏的开始、结束, 游戏玩家的加入、退出、行动等等.
class BaseGame {
    // using boost::signals2::signal< void() > OnCall;
    // using OnCall::slot_type                 OnCallSlot;

public:
    // max_player_num: 游戏最大人数
    BaseGame();

    // 为多态基类声明 vitural 析构函数
    virtual ~BaseGame();

    static BaseGamePtr Create() {
        return BaseGamePtr( new BaseGame() );
    }

    // 游戏初始化参数
    virtual void paramsInit( Args& args );

    // 往一局游戏中添加或删除一个玩家
    StandResponse         addPlayer( const std::string& account );
    virtual StandResponse onAddPlayer( AvatarPtr avatar );
    StandResponse         deletePlayer( const std::string& account );
    virtual StandResponse onDeletePlayer( const std::string& account );

    // 玩家行动
    StandResponse         playerAction( const std::string& account, int action_type, Args args );
    virtual StandResponse onPlayerAction( AvatarPtr avatar, BaseActionPtr action );

    // 整局游戏的开始和结束
    void         StartGame();
    void         EndGame();
    virtual void onStartGame();
    virtual void onEndGame();

    // 每次行动的开始和结束
    void         StartTurn();
    void         EndTurn();
    virtual void onStartTurn();
    virtual void onEndTurn();

    // 游戏结束判断
    StandResponse         checkGameFinish();
    virtual StandResponse onCheckGameFinish();

    // 行动结束判断
    StandResponse         checkTurnFinish();
    virtual StandResponse onCheckTurnFinish();

    std::map< std::string, AvatarPtr >& getPlayerMap() {
        return m_player_map;
    }
    GameStatus&              getStatus();
    Hexagonal::HexGridMapPtr getMap();
    AvatarPtr                getPlayer( const std::string& account );

    bool isEnd() {
        return m_status == GameStatus::END;
    }

private:
    // void _connection( OnCall& onCall, OnCallSlot& slot );
    // { onCall.connect( slot ); }

    int m_max_player_num;
    int m_current_player_number;

    GameStatus m_status;

    std::vector< std::string > m_color_list;

    std::map< std::string, AvatarPtr > m_player_map;

    Hexagonal::HexGridMapPtr m_hexgrid_map;

    // std::map< std::function< void() >, OnCall > m_call_map;

    // OnCall m_on_call;
};

#endif  // BASE_GAME_H_