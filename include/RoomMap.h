#ifndef ROOM_MAP_H_
#define ROOM_MAP_H_

#include "ClientObject.h"
#include "TurnBasedGame.h"
#include <map>

class GameManagerMap {
public:
    GameManagerMap();

    int CreateRoom( int type, int player_num );

    bool EnterRoom( int room_id, const ClientObject& client );

    bool CloseRoom( int room_id );

    bool ClearEndRoom();

    bool QuitRoom( int room_id, const ClientObject& client );

    TurnBasedGamePtr GetRoomPtr( int room_id );

    int next_room_id;

    std::mutex m_mutex;

    std::map< int, TurnBasedGamePtr > m_map;
};

#endif  // ROOM_MAP_H_