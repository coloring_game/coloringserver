#ifndef HEXAGONAL_H_
#define HEXAGONAL_H_

#include "../lib/SmartPtr.h"
#include <cmath>
#include <functional>
#include <iostream>
#include <map>
#include <memory>  //shared_ptr
#include <string>
#include <vector>

#include "Args.h"

#include <glog/logging.h>

namespace Hexagonal {
const int RADIUS = 6;

static const int         AVOID_TIMES   = 1;
static const std::string DEFAULT_COLOR = "white";

static const int CountTable[ 256 ] = { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4,
                                       3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5,
                                       3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5,
                                       4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
                                       3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8 };

class Hex {
public:
    Hex( int q_, int r_ );
    Hex( int q_, int r_, int s_ );
    Hex( double q_, double r_ );
    Hex( double q_, double r_, double s_ );
    friend std::ostream& operator<<( std::ostream& out, const Hex& hex );

    int q;
    int r;
    int s;
};

namespace HexFunction {

    Hex hex_add( const Hex& a, const Hex& b );

    Hex hex_subtract( const Hex& a, const Hex& b );

    Hex hex_scale( const Hex& a, int k );

    Hex hex_rotate_left( const Hex& a );

    Hex hex_rotate_right( const Hex& a );

    Hex hex_neighbor( const Hex& hex, int direction );

    Hex hex_diagonal_neighbor( const Hex& hex, int direction );

    int hex_length( const Hex& hex );

    int hex_distance( const Hex& a, const Hex& b );

    Hex hex_lerp( const Hex& a, const Hex& b, double t );

    std::vector< Hex > hex_linedraw( const Hex& a, const Hex& b );
}  // namespace HexFunction

class MyGrid {
public:
    MyGrid( int q, int r );

    MyGrid( const Hex& hex );

    void ParamsInit();

    void Active( std::string color, bool is_border = false );

    void FinishOneRound();

    bool IsNeedBorder();

    void SetBorder();

    void ClearBorder();

    std::string getStrHex();

    Hex         m_pos;
    int         m_avoid_times;
    std::string m_color;
    int         m_visited_times;
    bool        m_is_need_border;
};

class HexGridMap;
using HexGridMapPtr = SmartPtr< HexGridMap >;

class HexGridMap {
public:
    HexGridMap();

    ~HexGridMap();

    static HexGridMapPtr Create() {
        return HexGridMapPtr( new HexGridMap() );
    }

    std::vector< std::vector< MyGrid* > > GetMap();

    MyGrid* GetGrid( int q, int r );

    MyGrid* GetGrid( const Hex& hex );

    //清除某个颜色的行动标记
    void ClearMapBorder( std::string color );

    void SetMapColor( std::vector< MyGrid* > grid_vector, std::string color, bool check_avoid_times = true );

    unsigned long long get_now_millisecond();

    unsigned char dfs( int q, int r, std::string color, int flood_times );

    // 当设置某个颜色时, 判断是否形成包围
    // 如果从一个空白点出发, 只碰到了2条边界或2天以下边界, 就意味着被包围了
    bool CheckSurround( std::string color );

    // 内部函数, 用于遍历所有Grid
    void _IteratorAllGrid( std::function< void( int, int ) > func );

    void _ResetVisitedTimes( int q, int r );

    //清除某个颜色的行动标记
    void _ClearIsNeedBorder( int q, int r, std::string color );

    void _SetColorByFloodTimes( int q, int r, Args args );

    void _SubAvoidTimes( int q, int r );

    void SubAvoidTimes();

    bool CheckStartHex( const Hex& start_hex );

    // 各种道具
    std::vector< MyGrid* > Attack( const Hex& start_hex, int direction );

    std::vector< MyGrid* > Arrow( const Hex& start_hex, int direction, int length = 2 * m_radius );

    std::vector< MyGrid* > HexBlock( const Hex& start_hex, int length = 1 );

    std::vector< MyGrid* > Triangle( const Hex& start_hex, int direction, int length = 2 );

    int GridColorNumber( std::string color );

    std::vector< Hex > GridColorVector( std::string color );

    int GridNumber();

    static const int                      m_radius      = RADIUS;
    static const int                      m_grid_number = 1 + 3 * m_radius * ( m_radius + 1 );
    std::vector< std::vector< MyGrid* > > m_hexgrid_map;

    // 边界限制
    // 根据坐标Hex, 向grid_vector中插入m_hexgrid_map中对应的Grid*
    void _res_push_back( std::vector< MyGrid* >& grid_vector, const Hex& hex );

private:
};

}  // namespace Hexagonal
#endif  // HEXAGONAL_H_
