#ifndef AI_H_
#define AI_H_
#include "Args.h"
#include "Avatar.h"
#include "Hexagonal.h"
#include "TurnBasedGame.h"
#include <algorithm>
#include <cstdlib>
#include <random>
class AI;
using AIPtr = SmartPtr< AI >;
class AI {
public:
    AI( BaseGame* gm )
        : m_gm( gm ) {
        ;
    }

    static AIPtr Create( BaseGame* gm ) {
        return AIPtr( new AI( gm ) );
    }

    void firstRoundStrategy() {
        std::random_device rd;
        std::mt19937       g( rd() );

        Args args;

        std::vector< Hexagonal::Hex > hexs = getMap()->GridColorVector( "white" );
        std::shuffle( hexs.begin(), hexs.end(), g );
        for ( Hexagonal::Hex hex : hexs ) {
            Hexagonal::Hex neighbor = Hexagonal::HexFunction::hex_neighbor( hex, rand() % 6 );
            args.SetIntArgs( "q", neighbor.q );
            args.SetIntArgs( "r", neighbor.r );
            args.SetIntArgs( "direction", rand() % 6 );
            if ( m_gm->playerAction( "robot", 1, args ).m_ok ) {
                break;
            }
        }
    }

    void otherRoundStrategy() {
        std::random_device rd;
        std::mt19937       g( rd() );

        Args args;

        std::vector< Hexagonal::Hex > hexs = getMap()->GridColorVector( getPlayer( "robot" )->Color() );
        std::shuffle( hexs.begin(), hexs.end(), g );
        for ( Hexagonal::Hex hex : hexs ) {
            Hexagonal::Hex neighbor = Hexagonal::HexFunction::hex_neighbor( hex, rand() % 6 );
            args.SetIntArgs( "q", neighbor.q );
            args.SetIntArgs( "r", neighbor.r );
            args.SetIntArgs( "direction", rand() % 6 );
            if ( m_gm->playerAction( "robot", 1, args ).m_ok ) {
                break;
            }
        }
    }

    Hexagonal::HexGridMapPtr getMap() {
        return m_gm->getMap();
    }
    AvatarPtr getPlayer( std::string account ) {
        return m_gm->getPlayer( account );
    }

private:
    BaseGame* m_gm;
};

#endif  // AI_H_