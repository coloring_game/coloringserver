#ifndef ACTION_FACTORY_H_
#define ACTION_FACTORY_H_

#include "Actions/ArrowAction.h"
#include "Actions/Attack.h"
#include "Actions/BlockAction.h"
#include "BaseAction.h"
#include "Hexagonal.h"
using BaseActionPtr = SmartPtr< BaseAction >;

class ActionFactory {
public:
    static BaseActionPtr createAction( ActionType action_type, Hexagonal::HexGridMapPtr hexgrid_map, Args args );
    static BaseActionPtr createAction( int action_type, Hexagonal::HexGridMapPtr hexgrid_map, Args args );
};

#endif  // ACTION_FACTORY_H_