#ifndef CLIENT_OBJECT_H_
#define CLIENT_OBJECT_H_
#include "Timer.h"
#include "cinatra.hpp"
#include <glog/logging.h>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
using namespace cinatra;
class ClientObject {
public:
    ClientObject( std::string account );

    bool operator==( ClientObject client );

    void SetTimer( Timer* timer );

    Timer*& GetTimer();

    void SetColor( std::string color );

    const std::string& Color() const;

    const std::string& Account() const;

    void SetConnection( request& req );

    bool HasConnection();

    void ClearConnection();

    void SendMessage( std::string );

private:
    std::string                                      m_account;
    std::string                                      m_color;
    bool                                             is_can_do;
    Timer*                                           m_timer;
    std::shared_ptr< connection< cinatra::NonSSL > > m_connection;
};

class ClientMap {
public:
    ClientMap();

    ClientObject* get( std::string account, bool is_need_check_connection = true );

    bool find( std::string account );

    bool set( std::string account, ClientObject* client );

    bool remove( std::string account );

    void sendMsg( std::string msg );

private:
    std::mutex m_mutex;

    std::map< std::string, ClientObject* > m_map;
};

#endif  // CLIENT_OBJECT_H_