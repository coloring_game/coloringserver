#ifndef BLOCK_ACTION_H_
#define BLOCK_ACTION_H_

#include "BaseAction.h"
#include "StartHexAction.h"
class BlockAction : public StartHexAction {
public:
    BlockAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args );
    ~BlockAction();
    virtual StandResponse onDoAction() override;

    virtual const std::string getName() override {
        return "BlockAction";
    }
    virtual const ActionType getActionType() override {
        return ActionType::BlockAction;
    }

private:
};

#endif  // BLOCK_ACTION_H_