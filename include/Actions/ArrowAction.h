#ifndef ARROW_ACTION_H_
#define ARROW_ACTION_H_

#include "BaseAction.h"
#include "StartHexAction.h"

class ArrowAction : public StartHexAction {
public:
    ArrowAction( Hexagonal::HexGridMapPtr hexgrid_map, Args args );
    ~ArrowAction();
    virtual StandResponse onDoAction() override;

    virtual const std::string getName() override {
        return "ArrowAction";
    }
    virtual const ActionType getActionType() override {
        return ActionType::ArrowAction;
    }

private:
};

#endif  // ARROW_ACTION_H_