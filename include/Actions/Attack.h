#ifndef ATTACK_H_
#define ATTACK_H_

#include "BaseAction.h"
#include "StartHexAction.h"
class Attack : public StartHexAction {
public:
    Attack( Hexagonal::HexGridMapPtr hexgrid_map, Args args );
    ~Attack();
    virtual StandResponse onDoAction() override;

    virtual const std::string getName() override {
        return "Attack";
    }
    virtual const ActionType getActionType() override {
        return ActionType::Attack;
    }

private:
};

#endif  // ATTACK_H_