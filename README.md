# Coloring (染色大作战) 服务端代码

多人、回合制、六边形网格、道具组合

## Demo版本
设计一个可以让多个客户端同时染色的游戏，服务器只开启一局游戏。
为了达到上面的目的，需要设计如下基础功能:
- [x] 设计基于 epoll 的服务端结构, 用于处理每个客户端发送的 TCP/UDP 包
- [ ] 在 TCP/UDP 层的基础上, 设计一个 RPC 模块, 用于实现客户端、服务器之间互传协议
- [ ] 实现 RPC 模块的重点在于如何设计序列化、反序列化的功能. 由于游戏是回合制, 实时性不强, 因此只在字节层面解析 TCP/UDP 包, 并且只设计基本数据类型的序列化、反序列化功能.

## 初版
### 游戏玩法功能实现
- [ ] 回合制玩法
- [ ] 道具功能
- [ ] 回放功能 (Block 的 action_list 组合)
### 网游基础功能
- [ ] 注册、登录
- [ ] 游戏大厅: 可以开启多局游戏

## 代码实现逻辑
代码中有3个Service, 分别为:
### RoleService
用于用户的注册、登录, 其他 Service 可通过调用 RoleService 的方法来判断用户的合法性. 
其中有一个对应的 ClientMap 用来存储当前游戏中所有的用户
### RoomService
用于房间的创建、进入、删除, 每个 Room 其实就是一个 GameManager.
由它来进行游戏是否开始、轮到哪个玩家行动、游戏是否结束等判断, 同时还能把玩家的操作分发到对应的MaterialService.
其中有一个 RoomMap 用来存储当前游戏中所有的 Room

### MaterialService
用于实现对六边形网格地图的操作. 可能有若干个 MaterialService.

```mermaid
sequenceDiagram
    client ->> + RoleService : login/register
    	RoleService ->> RoleService : ClientMap.set(client)
    RoleService ->> - client : token
    client ->> + RoomService : CreateRoom()
        RoomService ->> +MaterialService : create a GameManager as gm
            MaterialService ->> MaterialService : init HexGridMap
        MaterialService ->> -RoomService : 
        RoomService ->> RoomService : RoomMap.set(gm)
    RoomService ->> -client : room_id
    client ->> +RoomService : EnterRoom(room_id)
        RoomService ->> RoomService : gm = RoomMap.get(room_id)
        RoomService ->> RoleService : checkPlayerOnline
        RoleService ->> RoomService : 
        RoomService ->> RoomService : gm.addPlayer(player)
        RoomService ->> RoomService : gm.StartGame()
    RoomService ->> -client : MaterialServiceAddr
    client ->> +RoomService : Attack(args)
    	RoomService ->> RoomService : check args
    	RoomService ->> MaterialService : Attack(args)
    	MaterialService ->> RoomService : 
    RoomService ->> -client : 
    client ->> +MaterialService : GetMap()/MaterialServiceAddr
    MaterialService ->> -client : MapInfo
    
```